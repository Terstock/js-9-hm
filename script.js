// Теоретичні питання
// 1. Опишіть своїми словами що таке Document Object Model (DOM)

//це програмний інтерфейс, який представляє структуру документу у вигляді дерева.
// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?

//innreHTML дозволяє отримувати  HTML в елементі  у виді рядка і його повністю замінити, innerText же дозволяє отримувати або змінювати
//текстовий зміст елемента, враховуючи відображений текст на сторінці.
// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?

//До елемента можна звернутись так:

// document.getElementById("id") - через id елемента
// document.getElementsByClassName("className") - через ім'я класу
// document.getElementsByName("Name") - повертає живий NodeList
// document.getElementsByTagName("TagName") - Повертає живий HTMLCollection
// document.querySelector(".selector") - поверне перший елемент з цим селектором
// document.querySelectorAll(".selectors")  - поверне всі селектори
//найкращий спосіб - це звернутись за селектором
// 4. Яка різниця між nodeList та HTMLCollection?
//HTMLCollection - жива і може змінюватись так би мовити онлайн, а NodeList - статичний
// Практичні завдання
//  1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
//  Використайте 2 способи для пошуку елементів.
//  Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).

// let clasus = document.getElementsByClassName("feature");
// console.log(clasus);

// let clasus2 = document.querySelectorAll(".feature");
// console.log(clasus2);

let elementus = document.querySelectorAll(".feature");
elementus.forEach(function (elementus) {
  elementus.style.textAlign = "center";
});

//  2. Змініть текст усіх елементів h2 на "Awesome feature".

let h2s = document.querySelectorAll(".feature-title");
console.log(h2s);
h2s.forEach(function (h2s) {
  h2s.innerHTML = "Awesome feature";
});

//  3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".

let titles = document.querySelectorAll(".feature-title");
titles.forEach(function (titles) {
  titles.append("!");
});
